package entity

import (
	"gorm.io/gorm"
)

type DriverPassengerChat struct {
	gorm.Model
	ID                    uint
	DriverID              uint
	Driver                User `gorm:"foreignkey:driver_id;references:id"`
	PassengerID           uint
	Passenger             User `gorm:"foreignkey:passenger_id;references:id"`
	OrderID               uint `gorm:"unique"`
	IsArchived            uint8
	LastMessageTimestamp  *int
	LastMessageID         *uint  
	LastMessage           DriverPassengerChatMessage `gorm:"foreignkey:last_message_id;references:id"`
	DriverUnreadAmount    int
	PassengerUnreadAmount int
}
