package entity

import (
	"gorm.io/gorm"
)

type SupportChat struct {
	gorm.Model
	ID                            uint
	UserID                        uint `gorm:"unique"`
	User                          User `gorm:"foreignkey:user_id;references:id"`
	LastMessageTimestamp          *int
	SessionChangedTimestamp       *int
	LastMessageID                 *uint
	LastMessage                   SupportChatMessage `gorm:"foreignkey:last_message_id;references:id"`
	LastUserID                    *uint
	LastUser                      User `gorm:"foreignkey:last_user_id;references:id"`
	IsSessionClosed               uint8
	IssueResolvedRequestTimestamp *int
	UserUnreadAmount              int
	AdminUnreadAmount             int
}
