package entity

import (
	"gorm.io/gorm"
)

type SupportChatMessage struct {
	gorm.Model
	ID        uint
	ChatID    uint
	UserID    *uint
	User      User `gorm:"foreignkey:user_id;references:id"`
	Timestamp int     
	Message   *string `gorm:"type:text"`
	ImageUrl  *string `gorm:"type:text"`
	AudioUrl  *string `gorm:"type:text"`
	Type      string  `gorm:"type:text"`
}
