package models

import (
	"gitlab.com/xcar-models/chat/entity"
	"gitlab.com/xcar-models/chat/helpers"
)

type SupportChat struct {
	ID                            uint                `json:"id"`
	UserID                        uint                `json:"user_id"`
	User                          User                `json:"user"`
	LastMessageTimestamp          *int                `json:"last_message_timestamp"`
	SessionChangedTimestamp        *int                `json:"session_changed_timestamp"`
	LastMessageID                 *uint               `json:"last_message_id"`
	LastMessage                   SupportChatMessage  `json:"last_message"`
	LastUserID                    *uint               `json:"last_user_id"`
	LastUser                      User                `json:"last_user"`
	IsSessionClosed               bool                `json:"is_session_closed"`
	IssueResolvedRequestTimestamp *int                `json:"issue_resolved_request_timestamp"`
	UserUnreadAmount              int                 `json:"user_unread_amount"`
	AdminUnreadAmount             int                 `json:"admin_unread_amount"`
}

func GetSupportChat(supportChat entity.SupportChat) SupportChat {
	var supportChatModel SupportChat

	supportChatModel.ID = supportChat.ID
	supportChatModel.UserID = supportChat.UserID
	supportChatModel.User = GetUser(supportChat.User)
	supportChatModel.LastMessageTimestamp = supportChat.LastMessageTimestamp
	supportChatModel.SessionChangedTimestamp = supportChat.SessionChangedTimestamp
	supportChatModel.LastMessageID = supportChat.LastMessageID
	supportChatModel.LastMessage = GetSupportChatMessage(supportChat.LastMessage)
	supportChatModel.LastUserID = supportChat.LastUserID
	supportChatModel.LastUser = GetUser(supportChat.LastUser)
	supportChatModel.IsSessionClosed = helpers.Uint8ToBool(supportChat.IsSessionClosed)
	supportChatModel.IssueResolvedRequestTimestamp = supportChat.IssueResolvedRequestTimestamp
	supportChatModel.UserUnreadAmount = supportChat.UserUnreadAmount
	supportChatModel.AdminUnreadAmount = supportChat.AdminUnreadAmount

	return supportChatModel
}

func GetSupportChatArray(array []entity.SupportChat) []SupportChat {
	var arrayModel []SupportChat

	for _, arrayElement := range array {
		arrayModel = append(arrayModel, GetSupportChat(arrayElement))
	}

	return arrayModel
}
