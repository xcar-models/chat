package models

import (
	"gitlab.com/xcar-models/chat/entity"
	"gitlab.com/xcar-models/chat/helpers"
)

type DriverPassengerChat struct {
	ID                    uint                        `json:"id"`
	DriverID              uint                        `json:"driver_id"`
	Driver                User                        `json:"driver"`
	PassengerID           uint                        `json:"passenger_id"`
	Passenger             User                        `json:"passenger"`
	OrderID               uint                        `json:"order_id"`
	IsArchived            bool                        `json:"is_archived"`
	LastMessageTimestamp  *int                        `json:"last_message_timestamp"`
	LastMessageID         *uint                       `json:"last_message_id"`
	LastMessage           DriverPassengerChatMessage  `json:"last_message"`
	DriverUnreadAmount    int                         `json:"driver_unread_amount"`
	PassengerUnreadAmount int                         `json:"passenger_unread_amount"`
}

func GetDriverPassengerChat(driverPassengerChat entity.DriverPassengerChat) DriverPassengerChat {
	var driverPassengerChatModel DriverPassengerChat

	driverPassengerChatModel.ID = driverPassengerChat.ID
	driverPassengerChatModel.DriverID = driverPassengerChat.DriverID
	driverPassengerChatModel.Driver = GetUser(driverPassengerChat.Driver)
	driverPassengerChatModel.PassengerID = driverPassengerChat.PassengerID
	driverPassengerChatModel.Passenger = GetUser(driverPassengerChat.Passenger)
	driverPassengerChatModel.OrderID = driverPassengerChat.OrderID
	driverPassengerChatModel.IsArchived = helpers.Uint8ToBool(driverPassengerChat.IsArchived)
	driverPassengerChatModel.LastMessageTimestamp = driverPassengerChat.LastMessageTimestamp
	driverPassengerChatModel.LastMessageID = driverPassengerChat.LastMessageID
	driverPassengerChatModel.LastMessage = GetDriverPassengerChatMessage(driverPassengerChat.LastMessage)
	driverPassengerChatModel.DriverUnreadAmount = driverPassengerChat.DriverUnreadAmount
	driverPassengerChatModel.PassengerUnreadAmount = driverPassengerChat.PassengerUnreadAmount

	return driverPassengerChatModel
}

func GetDriverPassengerChatArray(array []entity.DriverPassengerChat) []DriverPassengerChat {
	var arrayModel []DriverPassengerChat

	for _, arrayElement := range array {
		arrayModel = append(arrayModel, GetDriverPassengerChat(arrayElement))
	}

	return arrayModel
}
