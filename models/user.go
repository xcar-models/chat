package models

import (
	"gitlab.com/xcar-models/chat/entity"
)

type User struct {
	ID                        uint    `json:"id"`
	CityID                    uint    `json:"city_id"`
	PhoneNumber               string  `json:"phone_number"`
	Name                      string  `json:"name"`
	Role                      string  `json:"role"`
	ProfileUrl                *string `json:"profile_url"`
	Platform                  *string `json:"platform"`
	DeviceToken               *string `json:"device_token"`
	MqttToken                 *string `json:"mqtt_token"`
	IosNotificationsToken     *string `json:"ios_notifications_token"`
	AndroidNotificationsToken *string `json:"android_notifications_token"`
}

func GetUser(user entity.User) User {
	var userModel User

	userModel.ID = user.ID
	userModel.CityID = user.CityID
	userModel.PhoneNumber = user.PhoneNumber
	userModel.Name = user.Name
	userModel.Role = user.Role
	userModel.ProfileUrl = user.ProfileUrl
	userModel.Platform = user.Platform
	userModel.DeviceToken = user.DeviceToken
	userModel.MqttToken = user.MqttToken
	userModel.IosNotificationsToken = user.IosNotificationsToken
	userModel.AndroidNotificationsToken = user.AndroidNotificationsToken

	return userModel
}

func GetUserArray(array []entity.User) []User {
	var arrayModel []User

	for _, arrayElement := range array {
		arrayModel = append(arrayModel, GetUser(arrayElement))
	}

	return arrayModel
}
