package models

import (
	"gitlab.com/xcar-models/chat/entity"
)

type SupportChatMessage struct {
	ID        uint    `json:"id"`
	ChatID    uint    `json:"chat_id"`
	UserID    *uint   `json:"user_id"`
	User      User    `json:"user"`
	Timestamp int     `json:"timestamp"`
	Message   *string `json:"message"`
	ImageUrl  *string `json:"image_url"`
	AudioUrl  *string `json:"audio_url"`
	Type      string  `json:"type"`
}

func GetSupportChatMessage(supportChatMessage entity.SupportChatMessage) SupportChatMessage {
	var supportChatMessageModel SupportChatMessage

	supportChatMessageModel.ID = supportChatMessage.ID
	supportChatMessageModel.ChatID = supportChatMessage.ChatID
	supportChatMessageModel.UserID = supportChatMessage.UserID
	supportChatMessageModel.User = GetUser(supportChatMessage.User)
	supportChatMessageModel.Timestamp = supportChatMessage.Timestamp
	supportChatMessageModel.Message = supportChatMessage.Message
	supportChatMessageModel.ImageUrl = supportChatMessage.ImageUrl
	supportChatMessageModel.AudioUrl = supportChatMessage.AudioUrl
	supportChatMessageModel.Type = supportChatMessage.Type

	return supportChatMessageModel
}

func GetSupportChatMessageArray(array []entity.SupportChatMessage) []SupportChatMessage {
	var arrayModel []SupportChatMessage

	for _, arrayElement := range array {
		arrayModel = append(arrayModel, GetSupportChatMessage(arrayElement))
	}

	return arrayModel
}
